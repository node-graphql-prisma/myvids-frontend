import React, { Component } from 'react';
import VideoList from './VideoList';
import '../styles/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <VideoList />
      </div>
    );
  }
}

export default App;
