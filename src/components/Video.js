import React, { Component } from 'react';

class Video extends Component {
  render() {
    return (
      <div>
        <div>
        {this.props.video.title} - {this.props.video.description} ({this.props.video.url})
        </div>
      </div>
    )
  }
}

export default Video;
