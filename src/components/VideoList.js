import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Video from './Video';

class VideoList extends Component {
  render() {
    const FEED_QUERY = gql`
      {
        feed {
          videos {
            id
            title
            description
            url
            createdAt
          }
        }
      }
    `;

    return (
      <Query query={FEED_QUERY}>
        {({ loading, error, data }) => {
          if(loading) {
            return <div>Fetching data from server...</div>
          }
          if(error) {
            return <div>Error getting data from server...</div>
          }

          const videosToRender = data.feed.videos;
          return (
            <div>
              { videosToRender.map(vid => <Video key={vid.id} video={vid} />)}
            </div>  
          )
        }}
      </Query>
    )
  }
}

export default VideoList;
